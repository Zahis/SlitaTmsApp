package com.zislabtec.slitatms.app.main;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.GridBagLayout;
import javax.swing.JLabel;
import java.awt.Font;
import java.awt.Color;

public class SplashScreen extends JFrame {

	private static final long serialVersionUID = 8172057928884337490L;
	private JPanel contentPane;


	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SplashScreen frame = new SplashScreen();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public SplashScreen() {
		setUndecorated(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 652, 368);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblSlitaTms = new JLabel("SLITA TMS 1.0");
		lblSlitaTms.setForeground(Color.WHITE);
		lblSlitaTms.setFont(new Font("Agency FB", Font.BOLD, 34));
		lblSlitaTms.setBounds(31, 40, 187, 41);
		contentPane.add(lblSlitaTms);
		setLocationRelativeTo(null);
	}
}
